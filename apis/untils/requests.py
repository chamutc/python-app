# bin/python
"""
author: Cham Nguyen Phuong
Email: Chamutc@gmail.com
Create date: 2019-04-Nov 22:54
"""
import requests
from tqdm import tqdm
import logging
LOGGER = logging.getLogger(__name__)


class Request:
    def __init__(self, username=None, password=None):
        self.username = username
        self.password = password

    def download_file(self, url, destination_file):
        """
        support download file from url
        :param url:
        :param destination_file
        :return:
        """
        res = requests.get(url, auth=(self.username, self.password))
        total_size = int(res.headers.get('content-length', 0))
        # For 1KB
        block_size = 1024
        progress_bar = tqdm(total=total_size, unit='iB', unit_scale=True)
        with open(destination_file, 'wb') as code:
            for data in res.iter_content(block_size):
                progress_bar.update(len(data))
                code.write(data)
        progress_bar.close()
        return destination_file
