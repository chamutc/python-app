# bin/python
"""
author: Cham Nguyen Phuong
Email: Chamutc@gmail.com
Create date: 2019-04-Nov 22:54
"""
import logging


def init_logging(file_name=None, level=logging.DEBUG):
    loggers = logging.getLogger(__name__)
    # Create a Formatter for formatting the log messages
    logger_formatter = logging.Formatter('%(levelname)s : [%(funcName)s - %(lineno)d] - %(message)s')
    logging.basicConfig(format=logger_formatter, level=logging.DEBUG)
    if file_name:
        logger_handler = logging.FileHandler(filename=file_name)
        logger_handler.setLevel(logging.DEBUG)
        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)
        # Add the Handler to the Logger
        loggers.addHandler(logger_handler)
