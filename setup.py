from setuptools import setup

setup(
    name='apis',
    version='0.0.1',
    description='install a hello worl python lib',
    license='MIT',
    packages=['apis'],
    author='Chamnp',
    author_email='chamutc@gmail.com',
    keywords=['example'],
    url='https://gitlab.com/chamutc/python-app'
)