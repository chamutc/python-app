# bin/python
"""
author: Cham Nguyen Phuong
Email: Chamutc@gmail.com
Create date: 2019-04-Nov 22:54
"""
import posixpath
import logging
from apis.untils.log import init_logging
from apis.untils.requests import Request
LOGGER = logging.getLogger(__name__)


class DownloadFile:
    """
    Download file from url
    """
    def __init__(self, url, workspace):
        self.url = url
        self.workspace = workspace

    def run(self):
        des_file = posixpath.join(self.workspace, 'test.deb')
        req = Request()
        req.download_file(self.url, des_file)


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s : [%(funcName)s - %(lineno)d] - %(message)s', level=logging.DEBUG)
    LOGGER.info('begin download file')
    url = 'http://localhost:8081/api/v1/fs/temp/google-chrome-stable_current_amd64.deb?download'
    worskpace = '/home/chamnp/temp'
    d = DownloadFile(url, worskpace)
    d.run()
    LOGGER.info('download successful')
